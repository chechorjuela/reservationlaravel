<?php


namespace App\Infrastructure\Interfaces;


interface IBallotService
{
    public function findById(int $id);
    public function create($fields);
    public function update($data,$id);
    public function delete($id);
}
