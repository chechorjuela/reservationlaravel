<?php


namespace App\Infrastructure\Interfaces;


interface IBuyerService
{
  public function getAll();
  public function findById(int $id);
  public function create($data);
  public function update($id,$data);
  public function delete($id);
}
