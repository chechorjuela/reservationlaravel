<?php

namespace App\Infrastructure\Http\Controllers\Api;

use App\Domain\Contracts\BaseResponse as BaseResponse;
use App\Domain\Contracts\BuyerRequestContract as BuyerRequestContract;
use App\Infrastructure\Http\Controllers\Controller;
use App\Infrastructure\Services\BuyerService as BuyerService;
use Illuminate\Http\Request;

class BuyerController extends Controller
{
  private $response;
  private $buyerService;

  public function __construct(BaseResponse $baseResponse, BuyerService $buyerService)
  {
    $this->buyerService = $buyerService;
    $this->response = $baseResponse;
  }

  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\JsonResponse
   */
  public function index()
  {
    $data = $this->buyerService->getAll();
    return $this->response->returnResponse($data, 202);
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param \Illuminate\Http\Request $request
   * @return \Illuminate\Http\JsonResponse
   */
  public function store(BuyerRequestContract $request)
  {
    try {
      if ($request->validated()) {
        $data = $this->buyerService->create($request->all());
        return $this->response->returnResponse($data, 202);
      }

    } catch (\Exception $e) {
      return $this->response->returnResponse(null, 400, $e->getMessage());
    }
  }

  /**
   * Display the specified resource.
   *
   * @param int $id
   * @return \Illuminate\Http\JsonResponse
   */
  public function show($id)
  {
    $data = $this->buyerService->findById($id);

    return $this->response->returnResponse($data, 202);
  }

  /**
   * Update the specified resource in storage.
   *
   * @param \Illuminate\Http\Request $request
   * @param int $id
   * @return \Illuminate\Http\JsonResponse
   */
  public function update(BuyerRequestContract $request, $id)
  {
    try {
      if ($request->validated()) {
        $data = $this->buyerService->update($id, $request->all(),);
        return $this->response->returnResponse($data, 202);
      }

    } catch (\Exception $e) {
      return $this->response->returnResponse(null, 400, $e->getMessage());
    }
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param int $id
   * @return \Illuminate\Http\JsonResponse
   */
  public function destroy($id)
  {
    try {
      $data = $this->buyerService->delete($id);
      return $this->response->returnResponse($data, 202);
    } catch (\Exception $e) {
      return $this->response->returnResponse(null, 400, $e->getMessage());
    }
  }
}
