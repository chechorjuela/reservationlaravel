<?php

namespace App\Infrastructure\Http\Controllers\Api;

use App\Domain\Contracts\BallotRequestContract as BallotRequestContract;
use App\Domain\Contracts\BaseResponse as BaseResponse;
use App\Infrastructure\Http\Controllers\Controller;
use App\Infrastructure\Services\BallotService as BallotService;
use Illuminate\Http\Request;

class BallotController extends Controller
{
  private $response;
  private $ballotService;

  public function __construct(BaseResponse $baseResponse, BallotService $ballotService)
  {
    $this->ballotService = $ballotService;
    $this->response = $baseResponse;
  }

  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\JsonResponse
   */
  public function index()
  {
    $data = $this->ballotService->getAll();

    return $this->response->returnResponse($data, 202);
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param \Illuminate\Http\Request $request
   * @return \Illuminate\Http\JsonResponse
   */
  public function store(BallotRequestContract $request)
  {
    try {
      if ($request->validated()) {
        $data = $this->ballotService->create($request->all());
        return $this->response->returnResponse($data, 202);
      }

    } catch (\Exception $e) {
      return $this->response->returnResponse(null, 400, $e->getMessage());
    }
  }

  /**
   * Display the specified resource.
   *
   * @param int $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    $data = $this->ballotService->findById($id);

    return $this->response->returnResponse($data, 202);
  }


  /**
   * Update the specified resource in storage.
   *
   * @param \Illuminate\Http\Request $request
   * @param int $id
   * @return \Illuminate\Http\JsonResponse
   */
  public function update(BallotRequestContract $request, $id)
  {
    try {
      if ($request->validated()) {
        $data = $this->ballotService->update($request->all(), $id);
        return $this->response->returnResponse($data, 202);
      }

    } catch (\Exception $e) {
      return $this->response->returnResponse(null, 400, $e->getMessage());
    }
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param int $id
   * @return \Illuminate\Http\JsonResponse
   */
  public function destroy($id)
  {
    try {
      $data = $this->ballotService->delete($id);
      return $this->response->returnResponse($data, 202);
    } catch (\Exception $e) {
      return $this->response->returnResponse(null, 400, $e->getMessage());
    }

  }
}
