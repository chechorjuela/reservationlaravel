<?php

namespace App\Infrastructure\Http\Controllers\Api;

use App\Domain\Contracts\BaseResponse as BaseResponse;
use App\Domain\Contracts\ReservationRequestContract as ReservationRequestContract;
use App\Infrastructure\Http\Controllers\Controller;
use App\Infrastructure\Services\ReservationService as ReservationService;

class ReservationController extends Controller
{
  private $response;
  private $_reservationService;

  public function __construct(BaseResponse $baseResponse, ReservationService $reservationService)
  {
    $this->_reservationService = $reservationService;
    $this->response = $baseResponse;
  }

  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\JsonResponse
   */
  public function index()
  {
    $data = $this->_reservationService->getAll();
    return $this->response->returnResponse($data, 202);
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param \Illuminate\Http\Request $request
   * @return \Illuminate\Http\JsonResponse
   */
  public function store(ReservationRequestContract $request)
  {
    try {
      if ($request->validated()) {
        $data = $this->_reservationService->create($request->all());
        return $this->response->returnResponse($data, 202);
      }

    } catch (\Exception $e) {
      return $this->response->returnResponse(null, 400, $e->getMessage());
    }
  }

  /**
   * Display the specified resource.
   *
   * @param int $id
   * @return \Illuminate\Http\JsonResponse
   */
  public function show($id)
  {
    $data = $this->_reservationService->findById($id);

    return $this->response->returnResponse($data, 202);
  }

  /**
   * Update the specified resource in storage.
   *
   * @param \Illuminate\Http\Request $request
   * @param int $id
   * @return \Illuminate\Http\JsonResponse
   */
  public function update(ReservationRequestContract $request, $id)
  {
    try {
      if ($request->validated()) {
        $data = $this->_reservationService->update($id, $request->all(),);
        return $this->response->returnResponse($data, 202);
      }

    } catch (\Exception $e) {
      return $this->response->returnResponse(null, 400, $e->getMessage());
    }
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param int $id
   * @return \Illuminate\Http\JsonResponse
   */
  public function destroy($id)
  {
    try {
      $data = $this->_reservationService->delete($id);
      return $this->response->returnResponse($data, 202);
    } catch (\Exception $e) {
      return $this->response->returnResponse(null, 400, $e->getMessage());
    }
  }
}
