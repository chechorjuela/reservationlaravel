<?php


namespace App\Infrastructure\Services;


use App\Domain\Repository\ReservationRepository as ReservationRepository;
use App\Domain\Repository\BuyerRepository as BuyerRepository;
use App\Domain\Repository\BallotRepository as BallotRepository;
use App\Infrastructure\Interfaces\IBuyerService;

class ReservationService implements IBuyerService
{
  private $_reservationRepository;
  private $_buyerRepository;
  private $_ballotRepository;

  public function __construct(ReservationRepository $reservationRepository, BallotRepository $ballotRepository,
                              BuyerRepository $buyerRepository)
  {
    $this->_reservationRepository = $reservationRepository;
    $this->_buyerRepository = $buyerRepository;
    $this->_ballotRepository = $ballotRepository;
  }

  public function findById(int $id)
  {
    return $this->_reservationRepository->findById($id);
  }

  public function create($data)
  {
    if($this->_buyerRepository->findById($data['buyer_id'])){
      $ballot = $this->_ballotRepository->findById($data['ballot_id']);
      if($ballot){
        if($ballot->available>$data['amount']){
          $reservation = $this->_reservationRepository->create($data);
          if($reservation){
            $ballot->available = $ballot->available - $data['amount'];
            $ballot->dataSerialize($ballot);
            $this->_ballotRepository->update($ballot->id, $ballot->dataSerialize($ballot));
            return $reservation;
          }else{
            return 'error on sistem';
          }
        }else{
          return 'amount not available';
        }

      }
      else{
        return 'Ballot not exist';
      }
    }else{
      return 'Buyer not exist';
    }
  }

  public function getAll()
  {
    return $this->_reservationRepository->getAll();
  }

  public function update($id, $data)
  {
    return $this->_reservationRepository->update($id, $data);
  }

  public function delete($id)
  {
    $reservation = $this->_reservationRepository->findById($id);
    $ballot = $this->_ballotRepository->findById($reservation->ballot_id);
    $response = $this->_reservationRepository->remove($id);
    if($response){
      if($ballot){
        $ballot->available = $ballot->available + $reservation->amount;
        $this->_ballotRepository->update($ballot->id,$ballot->dataSerialize($ballot));
      }
    }
    return $response;
  }
}
