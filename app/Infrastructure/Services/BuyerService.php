<?php


namespace App\Infrastructure\Services;


use App\Domain\Repository\BuyerRepository as BuyerRepository;
use App\Infrastructure\Interfaces\IBuyerService;

class BuyerService implements IBuyerService
{
  private $_buyerRepository;

  public function __construct(BuyerRepository $buyerRepository)
  {
    $this->_buyerRepository = $buyerRepository;
  }
  public function findById(int $id)
  {
    return $this->_buyerRepository->findById($id);
  }

  public function create($data)
  {
    return $this->_buyerRepository->create($data);
  }

  public function getAll()
  {
    return $this->_buyerRepository->getAll();
  }

  public function update($id, $data)
  {
    return $this->_buyerRepository->update($id,$data);
  }

  public function delete($id)
  {
    return $this->_buyerRepository->remove($id);
  }
}
