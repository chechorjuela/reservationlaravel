<?php


namespace App\Infrastructure\Services;


use App\Domain\Repository\BallotRepository;
use App\Infrastructure\Interfaces\IBallotService;
use App\Infrastructure\Interfaces\id;

class BallotService implements IBallotService
{
  private $ballotRepository;

  public function __construct(BallotRepository $ballotRepository)
  {
    $this->ballotRepository = $ballotRepository;
  }
  public function getAll(){
    return $this->ballotRepository->getAll();
  }
  public function findById(int $id)
  {
    return $this->ballotRepository->findById($id);
  }

  public function create($fields)
  {
    return $this->ballotRepository->create($fields);
  }

  public function update($data, $id)
  {
    return $this->ballotRepository->update($id, $data);
  }

  public function delete($id)
  {
    return $this->ballotRepository->remove($id);
  }
}
