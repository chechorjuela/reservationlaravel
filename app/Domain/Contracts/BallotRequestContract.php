<?php


namespace App\Domain\Contracts;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Contracts\Validation\Validator;


class BallotRequestContract extends FormRequest
{
  public function authorize()
  {
    return true;
  }

  public function rules()
  {
    switch ($this->method()) {
      case 'PUT':
      case 'POST':
        return [
          'name' => 'required',
          'date_expiration' => 'required|date_format:Y-m-d',
          'amount' => 'required',
          'available' => 'required'
        ];
        break;
    }
  }


  /**
   * Get the error messages for the defined validation rules.
   *
   * @return array
   */
  public function messages()
  {
    return [
      'name.required' => 'El :attribute es requreido',
      'date_expiration.date_format' => 'La fecha de expiracion es incorrecta',
      'amount.required' => 'La proridad es obligatoria',
    ];
  }

  /**
   * @param Validator $validator
   */
  protected function failedValidation(Validator $validator)
  {
    $json = [];
    $json["code"] = 200;
    $json["data"] = $validator->errors();
    throw new HttpResponseException(response()->json($json, 422));
  }
}
