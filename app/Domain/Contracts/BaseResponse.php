<?php

namespace App\Domain\Contracts;


class BaseResponse
{
    private $response = ['Header'=>['status'=>'','message'=>''],'data'=>[]];
    private $status = 'success';

    public function returnResponse($data, $code, $message = null, $status = null)
    {
        if ($status != null)
            $this->status = $status;

        $this->response['Header']['status'] = $this->status;
        $this->response['data'] = $data;
        if (isset($message)):
            $this->response['Header']['message'] = $message;
        endif;
        return response()->json($this->response, $code);
    }
}
