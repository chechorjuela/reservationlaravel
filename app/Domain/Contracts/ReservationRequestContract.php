<?php


namespace App\Domain\Contracts;


use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class ReservationRequestContract extends FormRequest
{
  public function authorize()
  {
    return true;
  }

  public function rules()
  {
    return [
      'buyer_id' => 'required',
      'ballot_id' => 'required',
      'amount' => 'required',
    ];
  }


  /**
   * Get the error messages for the defined validation rules.
   *
   * @return array
   */
  public function messages()
  {
    return [
      'buyer_id.required' => 'El :attribute es requerido',
      'ballot_id.required' => 'El :attribute es requerido',
      'amount' => ':attribute es incorrecta',
    ];
  }

  /**
   * @param Validator $validator
   */
  protected function failedValidation(Validator $validator)
  {
    $json = [];
    $json["code"] = 200;
    $json["data"] = $validator->errors();
    throw new HttpResponseException(response()->json($json, 422));
  }
}
