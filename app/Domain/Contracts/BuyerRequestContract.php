<?php


namespace App\Domain\Contracts;


use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class BuyerRequestContract extends FormRequest
{
  public function authorize()
  {
    return true;
  }

  public function rules()
  {
    return [
      'firstname' => 'required',
      'lastname' => 'required',
      'email' => 'required',
    ];
  }


  /**
   * Get the error messages for the defined validation rules.
   *
   * @return array
   */
  public function messages()
  {
    return [
      'firstname.required' => 'El :attribute es requreido',
      'email' => ':attribute es incorrecta',
    ];
  }

  /**
   * @param Validator $validator
   */
  protected function failedValidation(Validator $validator)
  {
    $json = [];
    $json["code"] = 200;
    $json["data"] = $validator->errors();
    throw new HttpResponseException(response()->json($json, 422));
  }
}
