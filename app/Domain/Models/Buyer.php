<?php


namespace App\Domain\Models;


use Illuminate\Database\Eloquent\Model;

class Buyer extends Model
{
  protected $fillable = [
    'firstname', 'lastname', 'email'
  ];

  public function reservations()
  {
    return $this->hasMany(Reservation::class);
  }

}
