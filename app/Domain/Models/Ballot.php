<?php


namespace App\Domain\Models;


use Illuminate\Database\Eloquent\Model;

class Ballot extends Model
{
  protected $fillable = [
    'name', 'amount', 'date_expiration', 'available'
  ];
  public function reservations(){
    return $this->hasMany(Reservation::class);
  }
  public function dataSerialize($object): array
  {
    $dataArray = [
      'name' => $object->name,
      'date_expiration' => $object->date_expiration,
      'amount' => $object->amount,
      'available' => $object->available,
    ];
    return $dataArray;
  }
}
