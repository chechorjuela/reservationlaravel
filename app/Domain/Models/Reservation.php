<?php
namespace App\Domain\Models;

use Illuminate\Database\Eloquent\Model;

class Reservation extends Model
{
  protected $fillable = [
    'buyer_id', 'ballot_id', 'amount'
  ];
  public function buyer(){
    return $this->belongsTo(Buyer::class);
  }
  public function ballot(){
    return $this->belongsTo(Ballot::class);
  }
}
