<?php


namespace App\Domain\Repository;


interface IRepositoryModel
{
  public function getAll();

  public function findById($id);

  public function create($data);

  public function remove($id);

  public function update($id, $data);
}
