<?php


namespace App\Domain\Repository;


use App\Domain\Models\Buyer;

class BuyerRepository implements IRepositoryModel
{

  public function findById($id)
  {
    return Buyer::where('id',$id)->with('reservations')->get();
  }

  public function create($data)
  {
    Buyer::create($data);
    $buyer = Buyer::where($data)->orderBy('id', 'desc')->first();
    return $buyer;
  }

  public function remove($id)
  {
    $buyer = Buyer::find($id);
    if ($buyer) {
      if ($buyer->delete()) {
        return true;
      }
    }
    return false;
  }

  public function update($id, $data)
  {
    $buyer = Buyer::find($id);
    $buyer->fill($data);
    $buyer->save();
    return $buyer;
  }

  public function getAll()
  {
    return Buyer::with('reservations')->get();
  }
}
