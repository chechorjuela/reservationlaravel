<?php


namespace App\Domain\Repository;


use App\Domain\Models\Reservation;

class ReservationRepository implements IRepositoryModel
{

  public function getAll()
  {
    return Reservation::with('buyer','ballot')->get();
  }

  public function findById($id)
  {
    return Reservation::find($id);
  }

  public function create($data)
  {
    Reservation::create($data);
    $reservation = Reservation::where($data)->orderBy('id', 'desc')->first();
    return $reservation;
  }

  public function remove($id)
  {
    $reservation = Reservation::find($id);
    if ($reservation) {
      if ($reservation->delete()) {
        return true;
      }
    }
    return false;
  }

  public function update($id, $data)
  {
    // TODO: Implement update() method.
    $reservation = Reservation::find($id);
    $reservation->fill($data);
    $reservation->save();
    return $reservation;
  }
}
