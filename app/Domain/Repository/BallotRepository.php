<?php


namespace App\Domain\Repository;

use App\Domain\Models\Ballot;

class BallotRepository implements IRepositoryModel
{
  public function getAll(){
    return Ballot::with('reservations')->get();
  }
  public function findById($id)
  {
    return Ballot::find($id);
  }

  public function create($data)
  {
    Ballot::create($data);
    $ballot = Ballot::where($data)->orderBy('id', 'desc')->first();
    return $ballot;
  }

  public function remove($id)
  {

    $ballot = Ballot::find($id);
    if ($ballot) {
      if ($ballot->delete()) {
        return true;
      }
    }
    return false;
  }

  public function update($id, $data)
  {
    $ballot = Ballot::find($id);
    $ballot->fill($data);
    $ballot->save();
    return $ballot;
  }
}
