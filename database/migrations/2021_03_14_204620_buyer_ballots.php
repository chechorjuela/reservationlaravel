<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class BuyerBallots extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reservations', function (Blueprint $table) {
            $table->id();
            $table->integer('buyer_id')->unsigned();
            $table->integer('ballot_id')->unsigned();
            $table->integer('amount');
            $table->foreign('buyer_id')->references('id')->on('buyers');
            $table->foreign('ballot_id')->references('id')->on('ballots');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
